#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <arpa/inet.h>

unsigned int readbuf[8192/4];

int main(int argc, char *argv[])
{
	int i;
	FILE *fpi, *fpo;
	size_t rs, sum, pad;

	if (argc != 3) {
		printf("usage: pad512 infile outfile\n");
		return -2;
	}
	
	if ((fpi = fopen(argv[1], "r")) == NULL)
		return -1;
	fpo = fopen(argv[2], "w");
	
	sum = 0;
	while (1) {
		rs = fread(readbuf, 1, 8192, fpi);
		if (rs) {
			fwrite(readbuf, 1, rs, fpo);
			sum += rs;
		} else {
			break;
		}
	}
	fclose(fpi);
	pad = 512 - (sum & 511);
	memset(readbuf, 0, 512);
	fwrite(readbuf, 1, pad, fpo);
	fclose(fpo);

	return 0;
}
