#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <arpa/inet.h>

#define FASTBOOT_BOOT_MAGIC_SIZE 8
#define FASTBOOT_BOOTLDR_IMG_NUM 3

#define FASTBOOT_BOOTLOADER_MAGIC "BOOTLDR!"

struct fastboot_bootldr_img_hdr {
	unsigned char magic[FASTBOOT_BOOT_MAGIC_SIZE];

	unsigned int version;
	unsigned int image_num;
	struct {
		unsigned char name[12];
		unsigned start;
		unsigned size;
	}img_info[FASTBOOT_BOOTLDR_IMG_NUM];
};

#define START_OFFSET 0x100

struct {
	const char *fname;	// file name
	const char *pname;	// partition name
} image[FASTBOOT_BOOTLDR_IMG_NUM] = {
	{"u-boot.bin", "bootloader"},
	{"spl/koi-spl.bin", "bl2"},
	{"../../../vendor/casio/koi/prebuilts/bootlogo.bmp", "bootlogo"}
};




struct fastboot_bootldr_img_hdr hdr;
unsigned int readbuf[8192/4];

int main(int argc, char *argv[])
{
	int i;
	FILE *fpi, *fpo;
	long sz;
	size_t rs;
	unsigned sta = START_OFFSET;

	strncpy(hdr.magic, FASTBOOT_BOOTLOADER_MAGIC, 8);
	hdr.image_num = FASTBOOT_BOOTLDR_IMG_NUM;

	for (i = 0; i < FASTBOOT_BOOTLDR_IMG_NUM; i++) {
		fpi = fopen(image[i].fname, "r");
		if (fpi == NULL) {
			printf("%s not found\n", image[i].fname);
			return -1;
		}
		fseek(fpi, 0, SEEK_END);
		sz = ftell(fpi);
		fclose(fpi);
		hdr.img_info[i].size = (unsigned)sz;
		hdr.img_info[i].start = sta;
		sta += hdr.img_info[i].size;
	//	sta += 0x20;
	//	sta &= ~0x10;
		strcpy(hdr.img_info[i].name, image[i].pname);
	}

	fpo = fopen("bootloader.img", "w");
	fwrite(&hdr, 1, sizeof(hdr), fpo);
	
	for (i = 0; i < FASTBOOT_BOOTLDR_IMG_NUM; i++) {
		fpi = fopen(image[i].fname, "r");
		sz = hdr.img_info[i].size;
		printf("[%d]: start = %d, size = %d\n", i, 
			hdr.img_info[i].start, hdr.img_info[i].size);
		fseek(fpo, hdr.img_info[i].start, SEEK_SET);
		while (sz) {
			if (sz > 8192)
				rs = fread(readbuf, 1, 8192, fpi);
			else
				rs = fread(readbuf, 1, sz, fpi);
			fwrite(readbuf, 1, rs, fpo);
			sz -= rs;
		}
		fclose(fpi);
	}
	fclose(fpo);
	return 0;
}
