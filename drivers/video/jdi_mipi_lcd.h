/* Header file for jdi display driver */

#ifndef _JDI_MIPI_LCD_H
#define _JDI_MIPI_LCD_H

	static const unsigned char OPTIONAL_SETTING[][2] = {
		{0x02, 0x40},
		{0x03, 0x00},
		{0x04, 0x00},
		{0x05, 0x02},
		{0x06, 0x0B},
		{0x07, 0x06},
		{0x08, 0x07},
		{0x09, 0x12},
		{0x0A, 0x13},
		{0x0B, 0x10},
		{0x0C, 0x11},
		{0x0D, 0x14},
		{0x0E, 0x15},
		{0x0F, 0x22},
		{0x10, 0x23},
		{0x11, 0x24},
		{0x12, 0x25},
		{0x13, 0x00},
		{0x14, 0x00},
		{0x15, 0x00},
		{0x16, 0x00},
		{0x17, 0x00},
		{0x18, 0x00},
		{0x19, 0x00},
		{0x1A, 0x00},
		{0x1D, 0x00},
		{0x1E, 0x00},
		{0x1F, 0x00},
		{0x20, 0x02},
		{0x21, 0x0B},
		{0x22, 0x06},
		{0x23, 0x07},
		{0x24, 0x12},
		{0x25, 0x13},
		{0x26, 0x10},
		{0x27, 0x11},
		{0x28, 0x14},
		{0x29, 0x15},
		{0x2A, 0x02},
		{0x2B, 0x03},
		{0x2D, 0x04},
		{0x2F, 0x05},
		{0x30, 0x00},
		{0x31, 0x00},
		{0x32, 0x00},
		{0x33, 0x00},
		{0x34, 0x00},
		{0x35, 0x00},
		{0x36, 0x00},
		{0x37, 0x00},
		{0x39, 0x01},
		{0x3A, 0x10},
		{0x3B, 0xFB},
		{0x3D, 0xFB},
		{0x42, 0x9D},
		{0x43, 0xD8},
		{0x44, 0x51},
		{0x46, 0x02},
		{0x49, 0x19},
		{0x4A, 0x08},
		{0x4B, 0xF2},
		{0x4C, 0x00},
		{0x4D, 0x0D},
		{0x4E, 0x98},
		{0x73, 0xD4},
		{0x74, 0x1F},
		{0x75, 0x16},
		{0x76, 0x11},
		{0x77, 0x91},
		{0x78, 0x51},
		{0x7A, 0x41},
		{0x7B, 0x00},
		{0x7D, 0xF6},
		{0x80, 0x2D},
		{0x81, 0x00},
		{0x82, 0x01},
		{0x83, 0x32},
		{0x84, 0x41},
		{0x8A, 0x00},
		{0x8B, 0x00},
		{0x8C, 0x00},
		{0x8D, 0x00},
		{0x8E, 0x80},
		{0x90, 0x01},
		{0x92, 0x0A},
		{0x93, 0x00},
		{0x97, 0x13},
		{0x9B, 0x45},
		{0x9C, 0x37},
		{0x9D, 0x08},
		{0x9E, 0x08},
		{0x9F, 0x00},
		{0xA0, 0x00},
		{0xA2, 0x00},
		{0xA4, 0x11},
		{0xA5, 0x55},
		{0xA6, 0x33},
		{0xA7, 0x00},
		{0xA9, 0x66},
		{0xAA, 0x00},
		{0xAB, 0x00},
		{0xAC, 0x00},
		{0xAD, 0x00},
		{0xAE, 0x11},
		{0xAF, 0x44},
		{0xB0, 0x00},
		{0xB1, 0x22},
		{0xB2, 0xA9},
		{0xB3, 0x1C},
		{0xB4, 0x42},
		{0xB5, 0x00},
		{0xB6, 0x00},
		{0xB7, 0x00},
		{0xB8, 0x00},
		{0xB9, 0x00},
		{0xBA, 0x20},
		{0xBB, 0x00},
		{0xBD, 0x33},
		{0xBF, 0x40},
		{0xC0, 0x00},
		{0xC2, 0x02},
		{0xC8, 0x00},
		{0xCD, 0x09},
		{0xCE, 0x04},
		{0xCF, 0x09},
		{0xD0, 0x04},
		{0xD2, 0xF9},
		{0xD3, 0xF9},
		{0xD4, 0xF9},
		{0xD5, 0x01},
		{0xD8, 0x41},
		{0xD9, 0xF9},
		{0xDA, 0x01},
		{0xDB, 0xA3},
		{0xE4, 0x02},
		{0xE5, 0x24},
		{0xE6, 0x30},
		{0xE7, 0x00},
		{0xE8, 0x00},
		{0xE9, 0x64},
		{0xEA, 0x04},
		{0xEB, 0x64},
		{0xEC, 0x19},
		{0xED, 0x64},
		{0xEE, 0x19},
		{0xEF, 0x05},
		{0xF0, 0x10},
		{0xFB, 0x01},
		{0xFF, 0x20},
		{0x75, 0x00},
		{0x76, 0x00},
		{0x77, 0x01},
		{0x78, 0x4C},
		{0x79, 0x01},
		{0x7A, 0x54},
		{0x7B, 0x01},
		{0x7C, 0x5A},
		{0x7D, 0x01},
		{0x7E, 0x5F},
		{0x7F, 0x01},
		{0x80, 0x63},
		{0x81, 0x01},
		{0x82, 0x66},
		{0x83, 0x01},
		{0x84, 0x69},
		{0x85, 0x01},
		{0x86, 0x6C},
		{0x87, 0x01},
		{0x88, 0x79},
		{0x89, 0x01},
		{0x8A, 0x8A},
		{0x8B, 0x01},
		{0x8C, 0xA7},
		{0x8D, 0x01},
		{0x8E, 0xC1},
		{0x8F, 0x01},
		{0x90, 0xF6},
		{0x91, 0x02},
		{0x92, 0x27},
		{0x93, 0x02},
		{0x94, 0x28},
		{0x95, 0x02},
		{0x96, 0x65},
		{0x97, 0x02},
		{0x98, 0xAB},
		{0x99, 0x02},
		{0x9A, 0xE1},
		{0x9B, 0x03},
		{0x9C, 0x20},
		{0x9D, 0x03},
		{0x9E, 0x43},
		{0x9F, 0x03},
		{0xA0, 0x70},
		{0xA2, 0x03},
		{0xA3, 0x7A},
		{0xA4, 0x03},
		{0xA5, 0x89},
		{0xA6, 0x03},
		{0xA7, 0x98},
		{0xA9, 0x03},
		{0xAA, 0xA2},
		{0xAB, 0x03},
		{0xAC, 0xAC},
		{0xAD, 0x03},
		{0xAE, 0xC0},
		{0xAF, 0x03},
		{0xB0, 0xDE},
		{0xB1, 0x03},
		{0xB2, 0xFF},
		{0xB3, 0x00},
		{0xB4, 0x00},
		{0xB5, 0x01},
		{0xB6, 0x4C},
		{0xB7, 0x01},
		{0xB8, 0x54},
		{0xB9, 0x01},
		{0xBA, 0x5A},
		{0xBB, 0x01},
		{0xBC, 0x5F},
		{0xBD, 0x01},
		{0xBE, 0x63},
		{0xBF, 0x01},
		{0xC0, 0x66},
		{0xC1, 0x01},
		{0xC2, 0x69},
		{0xC3, 0x01},
		{0xC4, 0x6C},
		{0xC5, 0x01},
		{0xC6, 0x79},
		{0xC7, 0x01},
		{0xC8, 0x8A},
		{0xC9, 0x01},
		{0xCA, 0xA7},
		{0xCB, 0x01},
		{0xCC, 0xC1},
		{0xCD, 0x01},
		{0xCE, 0xF6},
		{0xCF, 0x02},
		{0xD0, 0x27},
		{0xD1, 0x02},
		{0xD2, 0x28},
		{0xD3, 0x02},
		{0xD4, 0x65},
		{0xD5, 0x02},
		{0xD6, 0xAB},
		{0xD7, 0x02},
		{0xD8, 0xE1},
		{0xD9, 0x03},
		{0xDA, 0x20},
		{0xDB, 0x03},
		{0xDC, 0x43},
		{0xDD, 0x03},
		{0xDE, 0x70},
		{0xDF, 0x03},
		{0xE0, 0x7A},
		{0xE1, 0x03},
		{0xE2, 0x89},
		{0xE3, 0x03},
		{0xE4, 0x98},
		{0xE5, 0x03},
		{0xE6, 0xA2},
		{0xE7, 0x03},
		{0xE8, 0xAC},
		{0xE9, 0x03},
		{0xEA, 0xC0},
		{0xEB, 0x03},
		{0xEC, 0xDE},
		{0xED, 0x03},
		{0xEE, 0xFF},
		{0xEF, 0x00},
		{0xF0, 0x00},
		{0xF1, 0x01},
		{0xF2, 0x49},
		{0xF3, 0x01},
		{0xF4, 0x4B},
		{0xF5, 0x01},
		{0xF6, 0x4D},
		{0xF7, 0x01},
		{0xF8, 0x50},
		{0xF9, 0x01},
		{0xFA, 0x54},
		{0xFF, 0x21},
		{0x00, 0x01},
		{0x01, 0x57},
		{0x02, 0x01},
		{0x03, 0x5B},
		{0x04, 0x01},
		{0x05, 0x5F},
		{0x06, 0x01},
		{0x07, 0x70},
		{0x08, 0x01},
		{0x09, 0x7F},
		{0x0A, 0x01},
		{0x0B, 0x9A},
		{0x0C, 0x01},
		{0x0D, 0xB5},
		{0x0E, 0x01},
		{0x0F, 0xE5},
		{0x10, 0x02},
		{0x11, 0x15},
		{0x12, 0x02},
		{0x13, 0x17},
		{0x14, 0x02},
		{0x15, 0x4D},
		{0x16, 0x02},
		{0x17, 0x8D},
		{0x18, 0x02},
		{0x19, 0xBB},
		{0x1A, 0x02},
		{0x1B, 0xF2},
		{0x1C, 0x03},
		{0x1D, 0x23},
		{0x1E, 0x03},
		{0x1F, 0x48},
		{0x20, 0x03},
		{0x21, 0x57},
		{0x22, 0x03},
		{0x23, 0x61},
		{0x24, 0x03},
		{0x25, 0x70},
		{0x26, 0x03},
		{0x27, 0x7A},
		{0x28, 0x03},
		{0x29, 0x8E},
		{0x2A, 0x03},
		{0x2B, 0xA2},
		{0x2D, 0x03},
		{0x2F, 0xDE},
		{0x30, 0x03},
		{0x31, 0xFF},
		{0x32, 0x00},
		{0x33, 0x00},
		{0x34, 0x01},
		{0x35, 0x49},
		{0x36, 0x01},
		{0x37, 0x4B},
		{0x38, 0x01},
		{0x39, 0x4D},
		{0x3A, 0x01},
		{0x3B, 0x50},
		{0x3D, 0x01},
		{0x3F, 0x54},
		{0x40, 0x01},
		{0x41, 0x57},
		{0x42, 0x01},
		{0x43, 0x5B},
		{0x44, 0x01},
		{0x45, 0x5F},
		{0x46, 0x01},
		{0x47, 0x70},
		{0x48, 0x01},
		{0x49, 0x7F},
		{0x4A, 0x01},
		{0x4B, 0x9A},
		{0x4C, 0x01},
		{0x4D, 0xB5},
		{0x4E, 0x01},
		{0x4F, 0xE5},
		{0x50, 0x02},
		{0x51, 0x15},
		{0x52, 0x02},
		{0x53, 0x17},
		{0x54, 0x02},
		{0x55, 0x4D},
		{0x56, 0x02},
		{0x58, 0x8D},
		{0x59, 0x02},
		{0x5A, 0xBB},
		{0x5B, 0x02},
		{0x5C, 0xF2},
		{0x5D, 0x03},
		{0x5E, 0x23},
		{0x5F, 0x03},
		{0x60, 0x48},
		{0x61, 0x03},
		{0x62, 0x57},
		{0x63, 0x03},
		{0x64, 0x61},
		{0x65, 0x03},
		{0x66, 0x70},
		{0x67, 0x03},
		{0x68, 0x7A},
		{0x69, 0x03},
		{0x6A, 0x8E},
		{0x6B, 0x03},
		{0x6C, 0xA2},
		{0x6D, 0x03},
		{0x6E, 0xDE},
		{0x6F, 0x03},
		{0x70, 0xFF},
		{0x71, 0x00},
		{0x72, 0x00},
		{0x73, 0x01},
		{0x74, 0x4A},
		{0x75, 0x01},
		{0x76, 0x4C},
		{0x77, 0x01},
		{0x78, 0x4E},
		{0x79, 0x01},
		{0x7A, 0x50},
		{0x7B, 0x01},
		{0x7C, 0x52},
		{0x7D, 0x01},
		{0x7E, 0x54},
		{0x7F, 0x01},
		{0x80, 0x59},
		{0x81, 0x01},
		{0x82, 0x5E},
		{0x83, 0x01},
		{0x84, 0x69},
		{0x85, 0x01},
		{0x86, 0x7B},
		{0x87, 0x01},
		{0x88, 0x97},
		{0x89, 0x01},
		{0x8A, 0xAE},
		{0x8B, 0x01},
		{0x8C, 0xDE},
		{0x8D, 0x02},
		{0x8E, 0x0B},
		{0x8F, 0x02},
		{0x90, 0x0C},
		{0x91, 0x02},
		{0x92, 0x41},
		{0x93, 0x02},
		{0x94, 0x84},
		{0x95, 0x02},
		{0x96, 0xAC},
		{0x97, 0x02},
		{0x98, 0xE8},
		{0x99, 0x03},
		{0x9A, 0x0A},
		{0x9B, 0x03},
		{0x9C, 0x3B},
		{0x9D, 0x03},
		{0x9E, 0x50},
		{0x9F, 0x03},
		{0xA0, 0x5C},
		{0xA2, 0x03},
		{0xA3, 0x62},
		{0xA4, 0x03},
		{0xA5, 0x75},
		{0xA6, 0x03},
		{0xA7, 0x84},
		{0xA9, 0x03},
		{0xAA, 0xAA},
		{0xAB, 0x03},
		{0xAC, 0xE1},
		{0xAD, 0x03},
		{0xAE, 0xFF},
		{0xAF, 0x00},
		{0xB0, 0x00},
		{0xB1, 0x01},
		{0xB2, 0x4A},
		{0xB3, 0x01},
		{0xB4, 0x4C},
		{0xB5, 0x01},
		{0xB6, 0x4E},
		{0xB7, 0x01},
		{0xB8, 0x50},
		{0xB9, 0x01},
		{0xBA, 0x52},
		{0xBB, 0x01},
		{0xBC, 0x54},
		{0xBD, 0x01},
		{0xBE, 0x59},
		{0xBF, 0x01},
		{0xC0, 0x5E},
		{0xC1, 0x01},
		{0xC2, 0x69},
		{0xC3, 0x01},
		{0xC4, 0x7B},
		{0xC5, 0x01},
		{0xC6, 0x97},
		{0xC7, 0x01},
		{0xC8, 0xAE},
		{0xC9, 0x01},
		{0xCA, 0xDE},
		{0xCB, 0x02},
		{0xCC, 0x0B},
		{0xCD, 0x02},
		{0xCE, 0x0C},
		{0xCF, 0x02},
		{0xD0, 0x41},
		{0xD1, 0x02},
		{0xD2, 0x84},
		{0xD3, 0x02},
		{0xD4, 0xAC},
		{0xD5, 0x02},
		{0xD6, 0xE8},
		{0xD7, 0x03},
		{0xD8, 0x0A},
		{0xD9, 0x03},
		{0xDA, 0x3B},
		{0xDB, 0x03},
		{0xDC, 0x50},
		{0xDD, 0x03},
		{0xDE, 0x5C},
		{0xDF, 0x03},
		{0xE0, 0x62},
		{0xE1, 0x03},
		{0xE2, 0x75},
		{0xE3, 0x03},
		{0xE4, 0x84},
		{0xE5, 0x03},
		{0xE6, 0xAA},
		{0xE7, 0x03},
		{0xE8, 0xE1},
		{0xE9, 0x03},
		{0xEA, 0xFF},
		{0xFF, 0x10}};

#endif /* _JDI_MIPI_LCD_H */
