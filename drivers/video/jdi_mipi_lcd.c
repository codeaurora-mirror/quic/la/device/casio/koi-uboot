/* linux/drivers/video/decon_drivers/jdi_mipi_lcd.c */

#include <common.h>
#include <asm/arch/mipi_dsim.h>

#include "exynos_mipi_dsi_lowlevel.h"
#include "exynos_mipi_dsi_common.h"
#include "jdi_mipi_lcd.h"

static void optional_setting(struct mipi_dsim_device *dsim)
{
	struct mipi_dsim_master_ops *ops = dsim->master_ops;
	int i;

	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM, 0xFF, 0x24);
	for (i = 0; i < ARRAY_SIZE(OPTIONAL_SETTING); i++) {
		ops->cmd_write(dsim, MIPI_DSI_GENERIC_SHORT_WRITE_2_PARAM,
				OPTIONAL_SETTING[i][0], OPTIONAL_SETTING[i][1]);
	}
}


static void jdi_power_sequence_on(struct mipi_dsim_device *dsim)
{
	struct mipi_dsim_master_ops *ops = dsim->master_ops;

	unsigned char PWRON_CMD_01[2]	= {0xFB, 0x01 };
	unsigned char PWRON_CMD_02[2]	= {0xFF, 0x20 };
	unsigned char PWRON_CMD_03[2]	= {0xFB, 0x01 };
	unsigned char PWRON_CMD_04[2]	= {0xFF, 0x21 };
	unsigned char PWRON_CMD_05[2]	= {0xFB, 0x01 };
	unsigned char PWRON_CMD_06[2]	= {0xFF, 0x24 };
	unsigned char PWRON_CMD_07[2]	= {0xFB, 0x01 };
	unsigned char PWRON_CMD_08[2]	= {0xFF, 0x10 };
	unsigned char PWRON_CMD_09[2]	= {0xB3, 0x02 };
	unsigned char PWRON_CMD_10[2]	= {0xBB, 0x10 };

	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE,
			0x01, 0x00);
	mdelay(20);

	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM, 
	    	PWRON_CMD_01[0], PWRON_CMD_01[1]);
	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM, 
	    	PWRON_CMD_02[0], PWRON_CMD_02[1]);
	ops->cmd_write(dsim, MIPI_DSI_GENERIC_SHORT_WRITE_2_PARAM, 
	    	PWRON_CMD_03[0], PWRON_CMD_03[1]);
	ops->cmd_write(dsim, MIPI_DSI_GENERIC_SHORT_WRITE_2_PARAM, 
	    	PWRON_CMD_04[0], PWRON_CMD_04[1]);
	ops->cmd_write(dsim, MIPI_DSI_GENERIC_SHORT_WRITE_2_PARAM, 
	    	PWRON_CMD_05[0], PWRON_CMD_05[1]);
	ops->cmd_write(dsim, MIPI_DSI_GENERIC_SHORT_WRITE_2_PARAM, 
	    	PWRON_CMD_06[0], PWRON_CMD_06[1]);
	ops->cmd_write(dsim, MIPI_DSI_GENERIC_SHORT_WRITE_2_PARAM, 
	    	PWRON_CMD_07[0], PWRON_CMD_07[1]);
	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM, 
	    	PWRON_CMD_08[0], PWRON_CMD_08[1]);
	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM, 
	    	PWRON_CMD_09[0], PWRON_CMD_09[1]);
	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM, 
		PWRON_CMD_10[0], PWRON_CMD_10[1]);

	optional_setting(dsim);

	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM,
		0x3A, 0x07);		
	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM,
		0x35, 0x02);		
	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE,
		0x11, 0x00);	
	mdelay(10);
//	ops->cmd_write(dsim, MIPI_DSI_DCS_LONG_WRITE,
//		MIPI_DCS_WRITE_MEMORY_START, 0x00);
//	ops->cmd_write(dsim, MIPI_DSI_DCS_LONG_WRITE,
//		MIPI_DCS_WRITE_MEMORY_CONTINUE, 0x00);
	mdelay(120);
}


static int jdi_mipi_lcd_panel_set(struct mipi_dsim_device *dsim)
{
	jdi_power_sequence_on(dsim);
	return 0;
}

static void jdi_mipi_lcd_display_enable(struct mipi_dsim_device *dsim)
{
	struct mipi_dsim_master_ops *ops = dsim->master_ops;

	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE,
		0x29, 0x00);
}

static struct mipi_dsim_lcd_driver jdi_mipi_lcd_dsim_ddi_driver = {
	.name = "jdi_mipi_lcd",
	.id = -1,

	.mipi_panel_init = jdi_mipi_lcd_panel_set,
	.mipi_display_on = jdi_mipi_lcd_display_enable,
};

void jdi_mipi_lcd_init(void)
{
	exynos_mipi_dsi_register_lcd_driver(&jdi_mipi_lcd_dsim_ddi_driver);
}
