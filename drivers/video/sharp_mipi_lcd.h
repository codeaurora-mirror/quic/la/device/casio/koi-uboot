/* Header file for sharp display driver */

#ifndef _SHARP_MIPI_LCD_H
#define _SHARP_MIPI_LCD_H

void sharp_mipi_power_sequence_off(struct mipi_dsim_device *dsim);

#endif /* _SHARP_MIPI_LCD_H */
