/* uboot/drivers/video/sharp_mipi_lcd.c */

#include <common.h>
#include <asm/arch/mipi_dsim.h>

#include "exynos_mipi_dsi_lowlevel.h"
#include "exynos_mipi_dsi_common.h"
#include "sharp_mipi_lcd.h"
#include "sharp_mipi_lcd_commands.h"

static void sharp_power_sequence_on(struct mipi_dsim_device *dsim)
{
	struct mipi_dsim_master_ops *ops = dsim->master_ops;
	int i;

	unsigned char PWRON_CMD_01[2]	= {0xFF, 0x24 };
	// SHARP_PWRON_CMDS1 (defined in the header file)
	// SHARP_PWRON_CMDS2 (defined in the header file)
	// SHARP_PWRON_CMDS3 (defined in the header file)
	unsigned char PWRON_CMD_02[2]	= {0x42, 0x21 };
	unsigned char PWRON_CMD_03[2]	= {0xFF, 0x10 };
	unsigned char PWRON_CMD_04[2]	= {0xB3, 0x00 };
	unsigned char PWRON_CMD_05[2]	= {0xBB, 0x10 };
	unsigned char PWRON_CMD_06[2]	= {0x3A, 0x07 }; // PIXEL FORMAT
	unsigned char PWRON_CMD_07[6]	= {0x3B, 0x00, 0x08, 0x08, 0x20, 0x20 };
	unsigned char PWRON_CMD_08[2]	= {0x35, 0x00 }; // TEAR ON
	unsigned char PWRON_CMD_09[2]	= {0x11, 0x00 }; // EXIT SLEEP MODE
	// WRITE PIXEL DATA (2C, 3C)

	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM,
			PWRON_CMD_01[0], PWRON_CMD_01[1]);
	udelay(100);

	for (i = 0; i < ARRAY_SIZE(SHARP_PWRON_CMDS1); i++) {
		ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM,
				SHARP_PWRON_CMDS1[i][0], SHARP_PWRON_CMDS1[i][1]);
	}
	udelay(100);

	for (i = 0; i < ARRAY_SIZE(SHARP_PWRON_CMDS2); i++) {
		ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM,
				SHARP_PWRON_CMDS2[i][0], SHARP_PWRON_CMDS2[i][1]);
	}
	udelay(100);

	for (i = 0; i < ARRAY_SIZE(SHARP_PWRON_CMDS3); i++) {
		ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM,
				SHARP_PWRON_CMDS3[i][0], SHARP_PWRON_CMDS3[i][1]);
	}
	udelay(100);

	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM,
			PWRON_CMD_02[0], PWRON_CMD_02[1]);
	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM,
			PWRON_CMD_03[0], PWRON_CMD_03[1]);
	udelay(100);
	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM,
			PWRON_CMD_04[0], PWRON_CMD_04[1]);
	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM,
			PWRON_CMD_05[0], PWRON_CMD_05[1]);
	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM,
			PWRON_CMD_06[0], PWRON_CMD_06[1]);
	ops->cmd_write(dsim, MIPI_DSI_DCS_LONG_WRITE,
			(unsigned int)PWRON_CMD_07, ARRAY_SIZE(PWRON_CMD_07));
	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE_PARAM,
			PWRON_CMD_08[0], PWRON_CMD_08[1]);

	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE, 
			PWRON_CMD_09[0], PWRON_CMD_09[1]);
	mdelay(130);
}

void sharp_mipi_power_sequence_off(struct mipi_dsim_device *dsim)
{
	struct mipi_dsim_master_ops *ops = dsim->master_ops;

	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE, 0x28, 0x00);
	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE, 0x10, 0x00);
	mdelay(110);
}

static int sharp_mipi_lcd_panel_set(struct mipi_dsim_device *dsim)
{
	sharp_power_sequence_on(dsim);
	return 0;
}

static void sharp_mipi_lcd_display_enable(struct mipi_dsim_device *dsim)
{
	struct mipi_dsim_master_ops *ops = dsim->master_ops;

	ops->cmd_write(dsim, MIPI_DSI_DCS_SHORT_WRITE,
		0x29, 0x00);
}

static struct mipi_dsim_lcd_driver sharp_mipi_lcd_dsim_ddi_driver = {
	.name = "sharp_mipi_lcd",
	.id = -1,

	.mipi_panel_init = sharp_mipi_lcd_panel_set,
	.mipi_display_on = sharp_mipi_lcd_display_enable,
};

void sharp_mipi_lcd_init(void)
{
	exynos_mipi_dsi_register_lcd_driver(&sharp_mipi_lcd_dsim_ddi_driver);
}
