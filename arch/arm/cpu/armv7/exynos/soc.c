/*
 * Copyright (c) 2010 Samsung Electronics.
 * Minkyu Kang <mk7.kang@samsung.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <asm/io.h>
#include <asm/arch/power.h>

void reset_cpu(ulong addr)
{
#if defined(CONFIG_RAMDUMP_MODE)
	writel(0x0, CONFIG_RAMDUMP_SCRATCH);
#endif
	writel(0x1, samsung_get_base_swreset());
}

#if defined(CONFIG_MACH_SHIRI) || defined(CONFIG_MACH_KOI)
void power_off_cpu(ulong ignored)
{
	struct exynos4_power *pmu = (struct exynos4_power *)EXYNOS4_POWER_BASE;
	uint ps_hold_val;

	printf("power off... \n\n\n");

	/* Exynos3250 Power Off */
	ps_hold_val = readl(&pmu->ps_hold_control);
	writel(ps_hold_val & ~(POWER_PS_HOLD_CONTROL_DATA), &pmu->ps_hold_control);

	/* loop forever and wait for power off to happen */
	while (1) {
		if (serial_tstc()) {
			serial_getc();
			break;
		}
	}
}
#endif
